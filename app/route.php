<?php

class Route
{
    private $_uri = array();
    private $_method = array();
    private $_params= array();

    public function add($uri, $method = null)
    {
        if (strpos($uri, '/:')) {
            $this->_uri[] = '/' . trim($uri, '/'); substr($uri, 0, strpos($uri, '/:'));
            $this->_params[] = true;
        } else {
            $this->_uri[] = '/' . trim($uri, '/');
            $this->_params[] = false;
        }

        if ($method != null) {
            $this->_method[] = $method;
        }
    }

    public function submit()
    {
        $useMethod = null;
        $uriGetParam = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '/';

        foreach ($this->_uri as $key => $value) {

            if ($value == '/') {
                if ($value == $uriGetParam) {
                    $useMethod = $this->_method[$key];
                    break;
                }
            } else if ($value == $uriGetParam) {
                $useMethod = $this->_method[$key];
                break;
            } else if ($this->_params[$key] == true) {
                
                // for uri and params => /product/:id
                $paramValue = substr($value, 0, strpos($value, '/:'));
                if (strlen($uriGetParam) > strlen($paramValue)) {
                    if (substr($uriGetParam, 0, strlen($paramValue)) == $paramValue) {
                        global $params;
                        global $main_url;
                        $main_url = '/' . explode('/', $value)[1];
                        $params[explode('/:', $value)[1]] = ltrim(substr($uriGetParam, strlen($paramValue), strlen($uriGetParam) - strlen($paramValue)), '/');
                        $useMethod = $this->_method[$key];
                        break;
                    }
                }
                
            }
        }
        
        $par = explode('/', ltrim($uriGetParam, '/'));
        
        if ($useMethod != null) {
            require $useMethod;
            $page = new Page();
            $page->run();
        }
        else
        {
            echo "<script>window.location = '/'</script>";
            die();
        }
    }
}