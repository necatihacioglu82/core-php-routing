<?php

class Page
{
    public function run()
    {
        require '../pages/header.php';
        require '../pages/home/home-page.php';
        require '../pages/footer.php';
    }
}