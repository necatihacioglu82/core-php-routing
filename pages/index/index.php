<?php

class Page
{
    public function run()
    {
        require '../pages/header.php';
        require '../pages/index/index-page.php';
        require '../pages/footer.php';
    }
}