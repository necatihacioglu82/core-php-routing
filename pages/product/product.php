<?php

class Page
{
    public function run()
    {
        require '../pages/header.php';
        require '../pages/product/product-page.php';
        require '../pages/footer.php';
    }
}