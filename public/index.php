<?php

require '../app/route.php';

$route = new Route();

$route->add('/', '../pages/index/index.php');
$route->add('/home', '../pages/home/home.php');
$route->add('/product/:id', '../pages/product/product.php');
$route->add('/contact', '../pages/contact/contact.php');

$route->submit();